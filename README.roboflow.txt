
Trash Detection - v1 2022-02-20 7:13pm
==============================

This dataset was exported via roboflow.ai on February 23, 2022 at 9:19 PM GMT

It includes 57 images.
Recycle are annotated in COCO format.

The following pre-processing was applied to each image:
* Auto-orientation of pixel data (with EXIF-orientation stripping)
* Resize to 416x416 (Stretch)

The following augmentation was applied to create 3 versions of each source image:
* 50% probability of horizontal flip
* 50% probability of vertical flip


